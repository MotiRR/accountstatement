package edu.vtb;

import edu.vtb.modules.Account;
import edu.vtb.modules.Wiring;
import edu.vtb.modules.Client;
import edu.vtb.repository.AccountDAO;
import edu.vtb.repository.ClientDAO;
import edu.vtb.repository.WiringDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


@SpringBootApplication
public class Main implements CommandLineRunner {

    @Autowired
    ClientDAO clientDAO;
    @Autowired
    AccountDAO accountDAO;
    @Autowired
    WiringDAO wiringDAO;


    @Override
    public void run(String[] args) throws Exception {
        Client client_1, client_2;
        Account account_1, account_2, account_3, account_4;

        account_1 = new Account();
        account_1.setId(1L);
        account_2 = new Account();
        account_2.setId(2L);
        client_1 = new Client();
        client_1.setName("Иван");
        client_1.setAccounts(Arrays.asList(account_1, account_2));
        account_1.setClient(client_1);
        account_2.setClient(client_1);
        clientDAO.save(client_1);

        account_3 = new Account();
        account_3.setId(3L);
        account_4 = new Account();
        account_4.setId(4L);
        client_2 = new Client();
        client_2.setName("Петр");
        client_2.setAccounts(Arrays.asList(account_3, account_4));
        account_3.setClient(client_2);
        account_4.setClient(client_2);
        clientDAO.save(client_2);



        /*int id_user = 4, id_authority = 1;
        System.out.format("Есть ли у пользователя с id = %d, полномочие с " +
                "id = %d " + userDAO.haveAuthority(id_user, id_authority) + "\n", id_user, id_authority);
        System.out.format("Список полномочий пользователя с id = %d\n", id_user);
        List list = userDAO.putAllAuthority(id_user);
        if (list.isEmpty()) System.out.format("Полномочий нет");
        else
            list.forEach(System.out::println);*/
        Random r = new Random();
        for(int i = 0; i < 11; i++){
            Wiring wiring = new Wiring();
            wiring.setDate(new Date(2020, 12, 12));
            wiring.setWriteOff((long) r.nextInt(3)+1);
            wiring.setEnrollment((long) r.nextInt(3)+1);
            wiring.setAmount((long) r.nextInt(99999)+1);
            wiringDAO.save(wiring);
        }

        long id_client = 1;
        wiringDAO.calculateAmount(id_client, new Date(2020, 12, 12));
        System.out.format("Сумма зачисления для клиента id = %d равна %d\n", id_client,
                wiringDAO.calculateAmount(id_client, new Date(2020, 12, 12)));

        System.out.println("Дата  | Тип списания | Корреспондирующий счет | Сумма операции | ");
        List<Object[]> list = wiringDAO.createStatement(id_client);
        for(Object[] o : list) {
            System.out.println(o[0] +" | "+ o[1]+ " | "+ o[2]+ " | "+ o[3]+ " | ");
        }


    }

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(Main.class, args);
    }
}
