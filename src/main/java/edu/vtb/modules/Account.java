package edu.vtb.modules;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Account {
    @Id
    private Long id;
    @ManyToOne
    private Client client;
}
