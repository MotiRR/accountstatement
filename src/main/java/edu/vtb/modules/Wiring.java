package edu.vtb.modules;

import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Data
public class Wiring {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private Date date;
    private Long writeOff;
    private Long enrollment;
    private Long amount;
}
