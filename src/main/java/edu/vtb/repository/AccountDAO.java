package edu.vtb.repository;

import edu.vtb.modules.Account;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
@Transactional
public class AccountDAO {
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void save(Account account) {
        em.persist(account);
    }
}
