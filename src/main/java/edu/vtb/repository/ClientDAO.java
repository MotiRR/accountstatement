package edu.vtb.repository;

import edu.vtb.modules.Client;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional
public class ClientDAO {
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void save(Client user) {
        em.persist(user);
    }

    @Transactional(readOnly = true)
    public boolean haveAuthority(long user_id, long authority_id) {
        return em.createQuery("Select u.id from User u " +
                "join u.roles r " +
                "join r.roleOfAuthorities ra " +
                "join ra.authority a " +
                "where u.id = :u_id and a.id = :a_id")
                .setParameter("u_id", user_id)
                .setParameter("a_id", authority_id)
                .getResultStream().count() > 0;
    }

    @Transactional(readOnly = true)
    public List putAllAuthority(long user_id) {
        return em.createQuery("Select distinct a.name from User u " +
                "join u.roles r " +
                "join r.roleOfAuthorities ra " +
                "join ra.authority a " +
                "where u.id = :u_id")
                .setParameter("u_id", user_id)
                .getResultList();
    }
}
