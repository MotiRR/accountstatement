package edu.vtb.repository;

import edu.vtb.modules.Wiring;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Date;
import java.util.List;

@Repository
@Transactional
public class WiringDAO {
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void save(Wiring wiring) {
        em.persist(wiring);
    }

    @Transactional
    public long calculateAmount(long id_client, Date date) {

        return (long) em.createQuery("Select  coalesce(sum(w.amount),0) from Wiring w " +
                "where w.enrollment in (select a.id from Client c " +
                "join c.accounts a where c.id = :id) and w.date = :date")
                .setParameter("id", id_client)
                .setParameter("date", date)
                .getResultList()
                .get(0);
    }

    @Transactional
    public List createStatement(long id_client) {

        List allList = em.createQuery("Select w.date, str(:type_enrollment), " +
                "w.writeOff, sum(w.amount) from Wiring w " +
                "where w.enrollment in (select a.id from Client c " +
                "join c.accounts a where c.id = :id) " +
                "group by w.date, str(:type_enrollment), w.writeOff")
                .setParameter("type_enrollment", "Зачислние")
                .setParameter("id", id_client)
                .getResultList();

        List list = em.createQuery("Select w.date, str(:type_writeOff), " +
                "w.enrollment, w.amount from Wiring w " +
                "where w.writeOff in (select a.id from Client c " +
                "join c.accounts a where c.id = :id) " +
                "group by w.date, str(:type_writeOff), w.writeOff")
                .setParameter("type_writeOff", "Списание")
                .setParameter("id", id_client)
                .getResultList();
        allList.addAll(list);
        return allList;

    }

}
